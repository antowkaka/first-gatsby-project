import React from "react"
import "./AuthorCard.css"

export const AuthorCard = (props) => {
  const { firstName, lastName, post, publishingAt } = props


  return (
    <div>
      <div className="full_name">
        <h2>{firstName}</h2>
        <h2>{lastName}</h2>
      </div>
      <p>{post.post}</p>
      <span className="date">{publishingAt}</span>
    </div>
  )
}
