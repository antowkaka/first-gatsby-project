import React from "react"
import { StaticQuery, graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import { AuthorCard } from "../components/AuthorCard/AuthorCard"

const IndexPage = () => (
  <Layout>
    <SEO title="Home"/>
    <StaticQuery
      query={graphql`
          {
            contentfulAuthor {
              firstName,
              lastName,
              post {
                post
              },
              publishingAt
            }
          }
        `}
      render={( { contentfulAuthor } ) => (
        <AuthorCard firstName={contentfulAuthor.firstName} lastName={contentfulAuthor.lastName} post={contentfulAuthor.post} publishingAt={contentfulAuthor.publishingAt}/>
      )}
    />
  </Layout>
)

export default IndexPage
